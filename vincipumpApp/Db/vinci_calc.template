#############Algorithm definitions for VINCI controls##############
# 1. Blowout detection
#Algorithm: The algorithm assumes that the sampling rate (δt) of P is equal to 1s or less. At each sampling time tn, δP shall be calculated = P(tn)-
#(tn-1) (and where tn-1 = tn-δt). Further, at each sampling time, δP shall be compared with a threshold value δPthresh. If the condition δP <= 
#δPthresh becomes true then:
#a)	alarm shall be raised, and
#b)	an emergency stop signal shall be sent to the VINCI

record(ao, "$(P)$(R)DeltaP_Tresh") {
    field(DTYP,"Soft Channel")
    field(DESC,"Delta Pressure Treshold")
}

record(calcout, "$(P)$(R)Delta_Press"){
    field(INPA,"$(P)$(R)InpA")
    field(INPB,"$(P)$(R)Pressure CPP")
    field(CALC, "ABS(A-B)")
    field(OUT, "$(P)$(R)OutA")
    field(OOPT, "Every Time")
    field(FLNK, "$(P)$(R)InpA")
} 
record(ai, "$(P)$(R)InpA") {
    field(DTYP,"Soft Channel")
    field(DESC,"Keep the previous value")
    field(INP, "$(P)$(R)Pressure")
}
record(ao, "$(P)$(R)OutA") {
    field(DTYP,"Soft Channel")
    field(DESC,"Output A value")
}
record(calcout, "$(P)$(R)Blowout_calc"){
    field(INPA,"$(P)$(R)OutA CPP")
    field(INPB,"$(P)$(R)DeltaP_Tresh CPP")
    field(CALC, "A<B")
    field(OOPT, "On Change")
    field(OUT, "$(P)$(R)Blowout_alarm")
} 
record(ao, "$(P)$(R)Blowout_alarm") {
    field(DTYP,"Soft Channel")
    field(DESC,"Blowout alarm")
}

# 2. Force estimation
#The initial implementation of this algorithm will be super simple, neglecting friction and just using pressure (P)*area (A) to calculate force (F). 

#P is to be taken from the current actual system pressure (Modbus word 10) in units of bar and A shall be defined according to the operating 
#configuration in units of cm2.

#The calculated force is to be calculated and displayed in units of kN according to: 

#F(kN) = P(bar)*A(cm2)*1e-2

#The mass equivalent of this force, given in metric tonnes shall be derived from F(kN) according to: 

#M(met. tonnes) = F(kN)/9.8 

record(ai, "$(P)$(R)Area") {
    field(DTYP,"Soft Channel")
    field(DESC,"Area for calculation")
}
record(calcout, "$(P)$(R)Force_Est"){
    field(INPA, "$(P)$(R)Area CPP")
    field(INPB, "$(P)$(R)Pressure CPP")
    field(CALC, "B*A*0.01")
    field(DESC, "Force estimation")
}
record(calcout, "$(P)$(R)Met_Tonnes"){
    field(INPA, "$(P)$(R)Force_Est CPP")
    field(CALC, "A/9.8")
    field(DESC, "Metric tonnes")
}
# 3. Leak detection
#Leak detection is a potentially useful software feature that will alert the user if a leak forms anywhere in the system. Such a leak could cause 
#hazardous pooling of oil and, once enough oil has been lost a resultant loss of pressure in the system, which could compromise the neutron 
#measurement. 

#The algorithm shall require knowledge of the pressure set point Pset (modbus 34), the current actual pressure Pact (modbus 10) and the current flow F 
#(modbus 6). In addition, a stabilization time Tstable, an acceptable pressure differential Pdiff and a flow threshold Fthresh shall be defined.

#The condition that the system is at the requested set point shall be:

#|Pset – Pact|<=Pdiff

#When this condition is met, a timer shall initiate and, after an elapsed time of Tstable the current flow F is queried and compared with the flow 
#threshold. Under the condition that 

#F >= Fthresh


#pressure set by the user
record(ao, "$(P)$(R)P_diff") {
}

#threshold written by user
record(ao, "$(P)$(R)Threshold") {
    field(VAL, "1")
}

#time written by user after which the error should rise e.g. 50sec
record(ao, "$(P)$(R)Timer_init") {
    field(VAL, "50")
}

#actual timer value
record(ao, "$(P)$(R)Counter") {
}

#leak error record
record(ao, "$(P)$(R)Leak_error") {
}

record(aSub,  "$(P)$(R)leak") {
  field(SCAN, "1 second")
  field(DESC, "Test if there is a leak")
  field(SNAM, "asub_timer")
# inputs:
  field(INPA, "$(P)$(R)Timer_init") 		    field(FTA,  "SHORT")
  field(INPB, "$(P)$(R)PM_Press_SP_rbv CPP")	field(FTB,  "FLOAT")
  field(INPC, "$(P)$(R)Pressure CPP")	        field(FTC,  "FLOAT")
  field(INPD, "$(P)$(R)Flow CPP")		        field(FTD,  "FLOAT")
  field(INPE, "$(P)$(R)Threshold")		        field(FTE,  "FLOAT")
  field(INPF, "$(P)$(R)P_diff CPP")		        field(FTF,  "FLOAT")
# outputs:
  field(OUTA, "$(P)$(R)Counter PP") 		    field(FTVA, "SHORT")
  field(OUTB, "$(P)$(R)Leak_error PP") 		    field(FTVB, "SHORT")
}
